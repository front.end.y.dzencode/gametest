export default {
  state: {
    homeData: [],
  },
  actions: {
    // HOME ACTIONS
    async homeResponse(ctx) {
      const homeArray = {
        banner: {
          url: require("../../assets/img/content/home/homeBg.jpg")
        }
      };
      ctx.commit("updateHomeResponse", homeArray);
    }
  },
  mutations: {
    // HOME MUTATIONS
    updateHomeResponse(state, homeArray) {
      state.homeData = homeArray;
    },
  },
  getters: {
    // HOME GETTERS
    homeResponse(state) {
      return state.homeData
    },
  },
};