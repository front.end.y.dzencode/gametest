import {createStore} from 'vuex'
import home from '@/store/modules/home'
import modal from '@/store/modules/modal'
export default createStore({
  modules: {
    home,
    modal
  }
});
